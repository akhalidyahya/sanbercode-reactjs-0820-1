//soal 1
let luasLingkaran = (jari) => {
	return 22 / 7 * jari * jari;
}
let kelilingLingkaran = (jari) => {
	return 2 * 22 / 7 * jari;
}

//soal 2
let kalimat = ""

let add = (string) => {
	kalimat+=`${string} `;
}

add('Saya');
add('adalah');
add('seorang');
add('frontend');
add('developer');

// console.log(kalimat);

//soal 3 
const newFunction = function literal(firstName, lastName){
  return {
    firstName: firstName,
    lastName: lastName,
    fullName: ()=>{
      console.log(`${firstName} ${lastName}`)
      return 
    }
  }
}
// newFunction("William", "Imoh").fullName();

//soal 4
const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}

const {firstName, lastName, destination, occupation, spell} = newObject;

// console.log(firstName, lastName, destination, occupation);

//soal 5
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west,...east];
//Driver Code
// console.log(combined)